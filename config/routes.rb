Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'setup/branches#index'


  namespace :setup do
    resource :branches do

    end

    namespace :api do
      resource :products do

      end
    end
  end
end
