var Setup = {

  initialize: function(){


    var module = angular.module('setups', ['product-module']);

    $(document).on('ready page:load', function(arguments) {
      angular.bootstrap(document.body, ['setups'])
    });

    function getParameters(functionStr) {
      var paramStr = functionStr.slice(functionStr.indexOf('(') + 1, functionStr.indexOf(')'));
      var params;
      if (paramStr) {
        params = paramStr.split(",");
      }
      var paramsT = [];
      for (var n = 0; params && n < params.length; n++) {
        paramsT.push(params[n].trim());
      }
      return paramsT;
    }
  }
}
